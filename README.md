# Experimenting MNIST data set with tensorflow

This repository is used to explore tensorflow by implementing the MNIST dataset. 


# What is different between tensorflow and numpy for matrix multiplication?

Numpy does the expensive operation outside the python by using other efficient language which might cause overhead of transferring data back and forth. Tensorflow define a graph and does all the operations outside python.