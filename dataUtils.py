import numpy as np
import pandas as pd
import os
import pickle
import csv
'''
This is a program to read mnist data for trainning model
'''

def _decode32(inputByte):
    # newbyteorder '>' denote big endian 
    # refer to https://docs.scipy.org/doc/numpy/reference/generated/numpy.dtype.newbyteorder.html
    dt = np.dtype(np.uint32).newbyteorder('>')
    # each time calling read(4) will continue the next 4 bytes sequentially, therefore it will process 4 bytes by 4 bytes
    result = np.frombuffer(inputByte.read(4), dtype=dt)[0]
    return result

def _decode8(inputByte):
    dt = np.dtype(np.uint8).newbyteorder('>')
    # each time calling read(4) will continue the next 4 bytes sequentially, therefore it will process 4 bytes by 4 bytes
    result = np.frombuffer(inputByte.read(1), dtype=dt)[0]
    return result

def train_to_csv(label_directory, image_directory):
    labels = retrieve_label(label_directory)
    label_num = len(labels)
    with open('{}.csv'.format(image_directory),'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        with open(image_directory, 'br') as data:
            magic_num = _decode32(data)
            print('Magic num is', magic_num)
            image_num = _decode32(data)
            print("Images numbers:", image_num)
            if image_num != label_num:
                raise Exception("Image and labels not match!")
            rows = _decode32(data)
            print("Images rows:", rows)
            cols = _decode32(data)
            print("Images cols:", cols)
            for i in range(image_num):
                temp_img = []
                for _ in range(rows*cols):
                    temp_img.append(_decode8(data))
                temp_img.append(labels[i])
                writer.writerow(temp_img)
                print(labels[i])
    


def retrieve_data(directory):
    with open(directory, 'br') as data:
        magic_num = _decode32(data)
        print('Magic num is', magic_num)
        image_num = _decode32(data)
        print("Train images numbers:", image_num)
        rows = _decode32(data)
        print("Train images rows:", rows)
        cols = _decode32(data)
        print("Train images cols:", cols)
        result = pd.DataFrame(columns=[i for i in range(rows*cols)])
        for i in range(image_num):
            temp_img = []
            for _ in range(rows*cols):
                    temp_img.append(_decode8(data))
            result.loc[i] = temp_img
        return result

def retrieve_label(directory):
    result = []
    with open(directory, 'br') as data:
        magic_num = _decode32(data)
        print('Magic num is', magic_num, 'for images')
        label_num = _decode32(data)
        print("Label numbers:", label_num)
        for _ in range(label_num):
            result.append(_decode8(data))
        return result

def assemble_dataframe_from_csv(directory='./dataset/train-images-idx3-ubyte.csv'):
    # result = pd.read_csv(directory).reset_index()
    df = pd.read_csv(directory, header=None)
    return df

dataset_dir = {
    'train_img': './dataset/train-images-idx3-ubyte', 
    'train_label': './dataset/train-labels-idx1-ubyte',
    'test_img': './dataset/t10k-images-idx3-ubyte',
    'test_label': './dataset/t10k-labels-idx1-ubyte'
}

@DeprecationWarning
def save_train_to_csv():
    train_label = retrieve_label(dataset_dir["train_label"])
    train_img = retrieve_data(dataset_dir["train_img"])
    train_img['label'] = train_label
    train_img.to_csv('./dataset/train_data.csv', sep=",")

@DeprecationWarning
def save_test_to_csv():
    train_label = retrieve_label(dataset_dir["test_label"])
    train_img = retrieve_data(dataset_dir["test_img"])
    train_img['label'] = train_label
    train_img.to_csv('./dataset/test_data.csv', sep=",")

def pickle_train():
    train_label = retrieve_label(dataset_dir['train_label'])
    train_img = retrieve_data(dataset_dir['train_img'])

def next_train_batch(batch=100):
    if not os.path.exists('./dataset/train-images-idx3-ubyte.csv'):
        raise Exception("Should invoke save method first!")
    print("Fetching", batch, "batches...")
    df = assemble_dataframe_from_csv()
    return df.sample(n=batch)

def next_test_batch(batch=100):
    if not os.path.exists('./dataset/t10k-images-idx3-ubyte.csv'):
        raise Exception("Should invoke save method first!")
    print("Fetching", batch, "batches...")
    df = assemble_dataframe_from_csv('t10k-images-idx3-ubyte.csv')
    return df.sample(n=batch)

if __name__ == '__main__':
    # print(next_train_batch(100)['image'])
    # print(assemble_dataframe_from_csv().head(10)['image'].dtype)
    # train_to_csv(dataset_dir["test_label"], dataset_dir["test_img"]) 
    print(assemble_dataframe_from_csv())