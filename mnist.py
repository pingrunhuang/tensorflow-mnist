import tensorflow as tf
import dataUtils
import pandas as pd

# a placeholder for images with 28*28 pixels from mnist dataset
x = tf.placeholder(tf.float32, [None, 784])

# each image is a 1 * 784 tensor, the mnist has 10 classes
w = tf.Variable(tf.zeros([784, 10]))
# bias
b = tf.Variable(tf.zeros([10]))

# output: processed by an activate function 
y = tf.nn.softmax(tf.matmul(x,w) + b)

# define the cross-entropy for calculatin the loss

# this is the true distribution -- the one-hot vector
y_ = tf.placeholder(tf.float32, [None, 10])
# the reduction_indices cause the sum function to add up by the second dimension
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)

    loop_num = 1000
    for i in range(loop_num):
        df = dataUtils.next_train_batch(100)
        batch_ys = pd.get_dummies(df.iloc[:,28*28])
        # right most is not included
        batch_xs = df.iloc[:,:28*28]
        sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})


    correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
    # reduce the 
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    df = dataUtils.assemble_dataframe_from_csv(directory='./dataset/t10k-images-idx3-ubyte.csv')
    test_ys = pd.get_dummies(df.iloc[:,28*28])
    test_xs = df.iloc[:,:28*28]
    print(sess.run(accuracy, feed_dict={x:test_xs, y:test_ys}))

    wrtier = tf.summary.FileWriter("/tmp/mnistdemo/1")
    wrtier.add_graph(sess)